package parser.lom;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Auxiliary class. It represents a learning object graph to be used in the ACO
 * algorithm
 */
public class Graph {

	/**
	 * Creates and initializes a graph
	 *
	 * @param hmap
	 * @param data
	 * @param arcWeight
	 * @param topic
	 */
	public Graph(HashMap<Integer, String> hmap, ArrayList<Double> data, double[][] arcWeight,
			ArrayList<String[]> topic) {
		this.nodeInfo = new HashMap<Integer, String>(hmap);
		this.nodeData = data;
		this.arcWeight = arcWeight;
		this.nodeTopic = topic;
	}

	/**
	 * Get the nodeInfo
	 *
	 * @return nodeInfo
	 */
	public HashMap<Integer, String> getNodeInfo() {
		return this.nodeInfo;
	}

	/**
	 * Get the nodeData
	 *
	 * @return nodeData
	 */
	public ArrayList<Double> getNodeData() {
		return this.nodeData;
	}

	/**
	 * Get the arcWeight
	 *
	 * @return arcWeight
	 */
	public double[][] getArcWeight() {
		return this.arcWeight;
	}

	/**
	 * Get the nodeTopic
	 *
	 * @return nodeTopic
	 */
	public ArrayList<String[]> getNodeTopic() {
		return this.nodeTopic;
	}

	/**
	 * HashMap that contains the id and the name of each node
	 */
	private HashMap<Integer, String> nodeInfo;

	/**
	 * ArrayList that contains the similarity rating of each node in relation to
	 * the desired topic
	 */
	private ArrayList<Double> nodeData;

	/**
	 * NxN matrix containing the weight of the arcs that connect the graph.
	 */
	private double[][] arcWeight;

	/**
	 * ArrayList that contains arrays of 15 attributes which represents the
	 * topic of each node (learning object).
	 */
	private ArrayList<String[]> nodeTopic;
}