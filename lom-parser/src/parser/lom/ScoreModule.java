package parser.lom;

import org.apache.commons.text.similarity.JaroWinklerDistance;

/**
 * Class that compares and scores learning objects. It utilizes a string
 * similarity algorithm implementation taken from the org.apache.commons.text
 * package
 */
public class ScoreModule {
	/**
	 * Initializes a string similarity algorithm object in order to use the
	 * algorithm
	 */
	private static JaroWinklerDistance distance = new JaroWinklerDistance();

	/**
	 * Performs a comparison between the data of a learning object and a
	 * specified topic in order to generate a similarity rating
	 * 
	 * @param topic
	 * @param resource
	 * @return similarityRating
	 */
	public static double compare(String[] topic, String[] resource) {
		double score = 0.0;
		int counter = 0;

		/* Attribute "title" */
		if (resource[0] != null) {
			score += titleCompare(topic[0], resource[0]);
		} else {
			counter++;
		}

		/* Attribute "language" */
		if (resource[1] != null) {
			if (topic[1].equals(resource[1])) {
				score += 1.0;
			}
		} else {
			counter++;
		}

		/* Attribute "description" */
		if (resource[2] != null) {
			score += descriptionCompare(topic[2], resource[2].split(" "));
		} else {
			counter++;
		}

		/* Attribute "keyword" */
		if (resource[3] != null) {
			score += keywordCompare(topic[3].split(","), resource[3].split(","));
		} else {
			counter++;
		}

		/* Attribute "coverage" */
		if (resource[4] != null) {
			if (topic[4].equals(resource[4])) {
				score += 0.25;
			}
		} else {
			counter++;
		}

		/* Attribute "format" */
		if (resource[5] != null) {
			score += equalCompare(topic[5].split(","), resource[5].split(","));
		} else {
			counter++;
		}

		/* Attribute "typicalAgeRange" */
		if (resource[6] != null) {
			score += ageCompare(Integer.parseInt(topic[6]), Integer.parseInt(resource[6]));
		} else {
			counter++;
		}

		/* Attribute "difficulty" */
		if (resource[7] != null) {
			score += Parameters.difficultyMap.get("{" + topic[7] + "," + resource[7] + "}");
		} else {
			counter++;
		}

		/* Attribute "duration" */
		if (resource[8] != null) {
			score += durationCompare(Integer.parseInt(topic[8]), Integer.parseInt(resource[8]));
		} else {
			counter++;
		}

		/* Attribute "interactivityLevel" */
		if (resource[9] != null) {
			score += Parameters.levelMap.get("{" + topic[9] + "," + resource[9] + "}");
		} else {
			counter++;
		}

		/* Attribute "semanticDensity" */
		if (resource[10] != null) {
			score += Parameters.levelMap.get("{" + topic[10] + "," + resource[10] + "}");
		} else {
			counter++;
		}

		/* Attribute "intendedUserRole" */
		if (resource[11] != null) {
			score += equalCompare(topic[11].split(","), resource[11].split(","));
		} else {
			counter++;
		}

		/* Attribute "context" */
		if (resource[12] != null) {
			score += equalCompare(topic[12].split(","), resource[12].split(","));
		} else {
			counter++;
		}

		/* Attribute "cognitiveProcess" */
		if (resource[13] != null) {
			score += equalCompare(topic[13].split(","), resource[13].split(","));
		} else {
			counter++;
		}

		/* Attribute "cost" */
		if (resource[12] != null) {
			if (topic[12].equals(resource[12])) {
				score += 0.25;
			}
		} else {
			counter++;
		}

		if (counter > 0) {
			return ((score / (15 - counter)) + (score / 15)) / 2;
		} else {
			return (score / (15));
		}
	}

	/**
	 * Compares two strings using a string similarity algorithm, if the
	 * comparison exceeds a predefined acceptance value the function returns
	 * 1.75, otherwise returns 0
	 * 
	 * @param topic
	 * @param resource
	 * @return score
	 */
	private static double titleCompare(String topic, String resource) {
		double auxValue = 0.0;
		auxValue = distance.apply(topic, resource);
		if (auxValue >= Parameters.attributeWeight) {
			return 1.75;
		} else {
			return 0.0;
		}
	}

	/**
	 * Compares one string with every word that conforms the other using a
	 * string similarity algorithm, if the value of any of the comparisons
	 * exceeds a predefined acceptance value the function returns it
	 * 
	 * @param topic
	 * @param resource
	 * @return score
	 */
	private static double descriptionCompare(String topic, String[] resource) {
		double auxValue = 0.0;
		for (int i = 0; i < resource.length; i++) {
			auxValue = (distance.apply(topic, resource[i]) > auxValue) ? distance.apply(topic, resource[i]) : auxValue;
		}
		if (auxValue >= Parameters.attributeWeight) {
			return auxValue;
		} else {
			return 0.0;
		}
	}

	/**
	 * Compares a set of keywords using a string similarity algorithm, if the
	 * value of any of the comparisons exceeds a predefined acceptance value the
	 * function returns 1.75, otherwise returns 0
	 * 
	 * @param topic
	 * @param resource
	 * @return score
	 */
	private static double keywordCompare(String[] topic, String[] resource) {
		for (int i = 0; i < topic.length; i++) {
			for (int j = 0; j < resource.length; j++) {
				if (distance.apply(topic[i], resource[j]) >= Parameters.attributeWeight) {
					return 1.75;
				}
			}
		}
		return 0.0;
	}

	/**
	 * Compares a set of keywords using the equals function, if the value of any
	 * of the comparisons is true the function returns 1.0, otherwise returns 0
	 * 
	 * @param topic
	 * @param resource
	 * @return score
	 */
	private static double equalCompare(String[] topic, String[] resource) {
		for (int i = 0; i < topic.length; i++) {
			for (int j = 0; j < resource.length; j++) {
				if (topic[i].equals(resource[j])) {
					return 1.0;
				}
			}
		}
		return 0.0;
	}

	/**
	 * Compares two integers, if the numbers are equal the function returns 1.0,
	 * if they differ by +/- 3 the function returns 0.5, otherwise returns 0
	 * 
	 * @param topic
	 * @param resource
	 * @return score
	 */
	private static double ageCompare(int topic, int resource) {
		int auxValue = Math.abs(topic - resource);
		if (auxValue == 0) {
			return 1.0;
		} else if (auxValue <= 3) {
			return 0.5;
		} else {
			return 0.0;
		}
	}

	/**
	 * Compares two integers, if the numbers are equal the function returns 1.0,
	 * if they differ by +/- 30 the function returns 0.5, otherwise returns 0
	 * 
	 * @param topic
	 * @param resource
	 * @return score
	 */
	private static double durationCompare(int topic, int resource) {
		int auxValue = Math.abs(topic - resource);
		if (auxValue == 0) {
			return 1.0;
		} else if (auxValue <= 30) {
			return 0.5;
		} else {
			return 0.0;
		}
	}

	/**
	 * The function receives two strings that represent two integers and returns
	 * a string that represents the average of the two numbers
	 * 
	 * @param topic
	 * @param resource
	 * @return average
	 */
	public static String averageString(String topic, String resource) {
		return Integer.toString((Integer.parseInt(topic) + Integer.parseInt(resource)) / 2);
	}

	/**
	 * The function receives two strings that represent two integers and returns
	 * a string that represents the sum of the two numbers
	 * 
	 * @param topic
	 * @param resource
	 * @return sum
	 */
	public static String sumString(String topic, String resource) {
		return Integer.toString((Integer.parseInt(topic) + Integer.parseInt(resource)));
	}
}