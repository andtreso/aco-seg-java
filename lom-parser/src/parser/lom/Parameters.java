package parser.lom;

import java.util.HashMap;

/**
 * Global parameters used to adjust the parser and the scoreModule
 */
public class Parameters {

	/**
	 * Acceptance value for the string similarity algorithm applied to the
	 * comparison of the title, description and keyword attributes
	 */
	public static double attributeWeight = 0.80;

	/**
	 * The score of a node can't be lower than this number, otherwise there's no
	 * need to include it in the algorithm
	 */
	public static double minScore = 0.400;

	/**
	 * The score of a node can't be higher than this number, otherwise there's
	 * no need to find a solution because the node itself would be the solution
	 */
	public static double maxScore = 0.850;

	/**
	 * Map used for reference when comparing the difficulty attributes between
	 * the learning object and the topic
	 */
	public static final HashMap<String, Double> difficultyMap;

	/**
	 * Map used for reference when comparing the interactivityLevel and
	 * semanticDensity attributes between the learning object and the topic
	 */
	public static final HashMap<String, Double> levelMap;

	static {
		difficultyMap = new HashMap<String, Double>();
		levelMap = new HashMap<String, Double>();

		difficultyMap.put("{very difficult,very difficult}", 1.0);
		difficultyMap.put("{very difficult,difficult}", 0.75);
		difficultyMap.put("{very difficult,medium}", 0.5);
		difficultyMap.put("{very difficult,easy}", 0.25);
		difficultyMap.put("{very difficult,very easy}", 0.0);
		difficultyMap.put("{difficult,very difficult}", 0.75);
		difficultyMap.put("{difficult,difficult}", 1.0);
		difficultyMap.put("{difficult,medium}", 0.75);
		difficultyMap.put("{difficult,easy}", 0.5);
		difficultyMap.put("{difficult,very easy}", 0.25);
		difficultyMap.put("{medium,very difficult}", 0.5);
		difficultyMap.put("{medium,difficult}", 0.75);
		difficultyMap.put("{medium,medium}", 1.0);
		difficultyMap.put("{medium,easy}", 0.75);
		difficultyMap.put("{medium,very easy}", 0.5);
		difficultyMap.put("{easy,very difficult}", 0.25);
		difficultyMap.put("{easy,difficult}", 0.5);
		difficultyMap.put("{easy,medium}", 0.75);
		difficultyMap.put("{easy,easy}", 1.0);
		difficultyMap.put("{easy,very easy}", 0.75);
		difficultyMap.put("{very easy,very difficult}", 0.0);
		difficultyMap.put("{very easy,difficult}", 0.25);
		difficultyMap.put("{very easy,medium}", 0.5);
		difficultyMap.put("{very easy,easy}", 0.75);
		difficultyMap.put("{very easy,very easy}", 1.0);

		levelMap.put("{very high,very high}", 1.0);
		levelMap.put("{very high,high}", 0.75);
		levelMap.put("{very high,medium}", 0.5);
		levelMap.put("{very high,low}", 0.25);
		levelMap.put("{very high,very low}", 0.0);
		levelMap.put("{high,very high}", 0.75);
		levelMap.put("{high,high}", 1.0);
		levelMap.put("{high,medium}", 0.75);
		levelMap.put("{high,low}", 0.5);
		levelMap.put("{high,very low}", 0.25);
		levelMap.put("{medium,very high}", 0.5);
		levelMap.put("{medium,high}", 0.75);
		levelMap.put("{medium,medium}", 1.0);
		levelMap.put("{medium,low}", 0.75);
		levelMap.put("{medium,very low}", 0.5);
		levelMap.put("{low,very high}", 0.25);
		levelMap.put("{low,high}", 0.5);
		levelMap.put("{low,medium}", 0.75);
		levelMap.put("{low,low}", 1.0);
		levelMap.put("{low,very low}", 0.75);
		levelMap.put("{very low,very high}", 0.0);
		levelMap.put("{very low,high}", 0.25);
		levelMap.put("{very low,medium}", 0.5);
		levelMap.put("{very low,low}", 0.75);
		levelMap.put("{very low,very low}", 1.0);
	}
}