package parser.lom;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.*;

/**
 * Class in charge of parsing the learning objects and transform the information
 * obtained into a graph
 *
 */
public class LomParser {

	/**
	 * Parses all the XML files contained in a directory and compares their
	 * information with a specified topic. Returns a graph of learning objects
	 * ready to be processed by ACO
	 * 
	 * @param filesPath
	 * @param topic
	 * @return graph
	 */
	public static Graph parseFiles(String filesPath, String[] topic) {
		int counter = 0;
		double score = 0.0;
		String[] resource = null;

		HashMap<Integer, String> auxMap = new HashMap<Integer, String>();
		ArrayList<Double> auxList = new ArrayList<Double>();
		ArrayList<String[]> auxTopic = new ArrayList<String[]>();

		File dir = new File(filesPath);
		if (dir.isDirectory()) {
			File[] xmlFiles = dir.listFiles(new FilenameFilter() {
				@Override
				public boolean accept(File folder, String name) {
					return name.toLowerCase().endsWith(".xml");
				}
			});
			if (xmlFiles.length > 0) {
				for (int i = 0; i < xmlFiles.length; i++) {
					resource = parseLOM(xmlFiles[i]);
					score = ScoreModule.compare(topic, resource);
					if (score >= Parameters.minScore) {
						auxMap.put(counter, xmlFiles[i].getName());
						auxList.add(score);
						auxTopic.add(resource);
						counter++;
						System.out.println(xmlFiles[i].getName() + ": " + score);
					}
				}
				return new Graph(auxMap, auxList, null, auxTopic);
			} else {
				System.out.println("ERROR: The program could not find any file in the specified path");
				return null;
			}
		} else {
			System.out.println("ERROR: The specified path does not exist");
			return null;
		}
	}

	/**
	 * Parses a XML file and returns the processed information in the form of a
	 * 15 attribute array
	 * 
	 * @param file
	 * @return resource
	 */
	public static String[] parseLOM(File file) {
		Element auxElement = null;
		String[] data = new String[15];

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		try {
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(file);
			doc.getDocumentElement().normalize();

			/* Attribute "title" */
			auxElement = (Element) doc.getElementsByTagName("lomes:title").item(0);
			if (auxElement != null) {
				data[0] = auxElement.getElementsByTagName("lomes:string").item(0).getTextContent().toLowerCase();
			}

			/* Attribute "language" */
			auxElement = (Element) doc.getElementsByTagName("lomes:language").item(0);
			if (auxElement != null) {
				data[1] = auxElement.getTextContent().toLowerCase();
			}

			/* Attribute "description" */
			auxElement = (Element) doc.getElementsByTagName("lomes:description").item(0);
			if (auxElement != null) {
				data[2] = auxElement.getElementsByTagName("lomes:string").item(0).getTextContent().toLowerCase();
			}

			/* Attribute "keyword" */
			auxElement = (Element) doc.getElementsByTagName("lomes:keyword").item(0);
			if (auxElement != null) {
				NodeList keywordList = auxElement.getElementsByTagName("lomes:string");
				for (int i = 0; i < keywordList.getLength(); i++) {
					if (i > 0) {
						data[3] += "," + keywordList.item(i).getTextContent().toLowerCase();
					} else {
						data[3] = keywordList.item(i).getTextContent().toLowerCase();
					}
				}
			}

			/* Attribute "coverage" */
			auxElement = (Element) doc.getElementsByTagName("lomes:coverage").item(0);
			if (auxElement != null) {
				data[4] = auxElement.getElementsByTagName("lomes:string").item(0).getTextContent().toLowerCase();
			}

			/* Attribute "format" */
			NodeList formatList = doc.getElementsByTagName("lomes:format");
			if (formatList != null) {
				try {
					for (int i = 0; i < formatList.getLength(); i++) {
						if (i > 0) {
							data[5] += "," + formatList.item(i).getTextContent().split("/")[1];
						} else {
							data[5] = formatList.item(i).getTextContent().split("/")[1];
						}
					}
				} catch (ArrayIndexOutOfBoundsException e) {
					data[5] = null;
				}
			}

			/* Attribute "typicalAgeRange" */
			auxElement = (Element) doc.getElementsByTagName("lomes:typicalAgeRange").item(0);
			if (auxElement != null) {
				String ageString = auxElement.getElementsByTagName("lomes:string").item(0).getTextContent();
				if (ageString.contains("-")) {
					try {
						String[] tokens = ageString.split("-");
						int auxValue = 0;
						for (int i = 0; i < tokens.length; i++) {
							auxValue += Integer.parseInt(tokens[i]);
						}
						data[6] = Integer.toString(auxValue / 2);
					} catch (NumberFormatException e) {
						data[6] = null;
					}
				} else {
					data[6] = ageString.replaceAll("\\D+", "");
					if (data[6].isEmpty()) {
						data[6] = null;
					}
				}
			}

			/* Attribute "difficulty" */
			auxElement = (Element) doc.getElementsByTagName("lomes:difficulty").item(0);
			if (auxElement != null) {
				data[7] = auxElement.getElementsByTagName("lomes:value").item(0).getTextContent();
			}

			/* Attribute "duration" */
			auxElement = (Element) doc.getElementsByTagName("lomes:duration").item(0);
			if (auxElement != null) {
				/* String to be scanned to find the pattern */
				int auxValue = 0;
				String durationString = auxElement.getTextContent();
				String[] pattern = { "(?<=PT)(.*?)(?=H)", "(?<=H)(.*?)(?=M)" };
				for (int i = 0; i < 2; i++) {
					/* Create a Pattern object */
					Pattern r = Pattern.compile(pattern[i]);
					/* Now create matcher object */
					Matcher m = r.matcher(durationString);
					if (m.find()) {
						if (i == 0) {
							auxValue += 60 * (Integer.parseInt(m.group(0)));
						} else {
							auxValue += (Integer.parseInt(m.group(0)));
						}
					}
				}
				if (auxValue > 0) {
					data[8] = Integer.toString(auxValue);
				} else {
					data[8] = null;
				}
			}

			/* Attribute "interactivityLevel" */
			auxElement = (Element) doc.getElementsByTagName("lomes:interactivityLevel").item(0);
			if (auxElement != null) {
				data[9] = auxElement.getElementsByTagName("lomes:value").item(0).getTextContent();
			}

			/* Attribute "semanticDensity" */
			auxElement = (Element) doc.getElementsByTagName("lomes:semanticDensity").item(0);
			if (auxElement != null) {
				data[10] = auxElement.getElementsByTagName("lomes:value").item(0).getTextContent();
			}

			/* Attribute "intendedEndUserRole" */
			NodeList intendedEndUserRoleList = doc.getElementsByTagName("lomes:intendedEndUserRole");
			if (intendedEndUserRoleList != null) {
				for (int i = 0; i < intendedEndUserRoleList.getLength(); i++) {
					if (i > 0) {
						auxElement = (Element) intendedEndUserRoleList.item(i);
						data[11] += "," + auxElement.getElementsByTagName("lomes:value").item(0).getTextContent();
					} else {
						auxElement = (Element) intendedEndUserRoleList.item(i);
						data[11] = auxElement.getElementsByTagName("lomes:value").item(0).getTextContent();
					}
				}
			}

			/* Attribute "context" */
			NodeList contextList = doc.getElementsByTagName("lomes:context");
			if (contextList != null) {
				for (int i = 0; i < contextList.getLength(); i++) {
					if (i > 0) {
						auxElement = (Element) contextList.item(i);
						data[12] += "," + auxElement.getElementsByTagName("lomes:value").item(0).getTextContent();
					} else {
						auxElement = (Element) contextList.item(i);
						data[12] = auxElement.getElementsByTagName("lomes:value").item(0).getTextContent();
					}
				}
			}

			/* Attribute "cognitiveProcess" */
			NodeList cognitiveProcessList = doc.getElementsByTagName("lomes:cognitiveProcess");
			if (cognitiveProcessList != null) {
				for (int i = 0; i < cognitiveProcessList.getLength(); i++) {
					if (i > 0) {
						auxElement = (Element) cognitiveProcessList.item(i);
						data[13] += "," + auxElement.getElementsByTagName("lomes:value").item(0).getTextContent();
					} else {
						auxElement = (Element) cognitiveProcessList.item(i);
						data[13] = auxElement.getElementsByTagName("lomes:value").item(0).getTextContent();
					}
				}
			}

			/* Attribute "cost" */
			auxElement = (Element) doc.getElementsByTagName("lomes:cost").item(0);
			if (auxElement != null) {
				data[14] = auxElement.getElementsByTagName("lomes:value").item(0).getTextContent();
			}

		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return data;
	}
}