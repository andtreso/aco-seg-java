package aco;

/**
 * Global parameters used to adjust the ACO algorithm
 */
public class Parameters {
	/**
	 * The score of a node can't be higher than this number, otherwise there's
	 * no need to find a solution because the node itself would be the solution
	 */
	public static double maxScore = 0.850;

	/**
	 * Pheromone evaporation rate
	 */
	public static double rho = 0.5;

	/**
	 * Pheromone importance
	 */
	public static double alpha = 1.0;

	/**
	 * Heuristic importance
	 */
	public static double beta = 2.0;

	/**
	 * Size of ant population
	 */
	public static int antPopSize = 20;

	/**
	 * Number of iterations to find a good solution
	 */
	public static int iterationsMax = 150;

	/**
	 * Percentage to be used in order to decide if an Ant finishes the tour
	 * early
	 */
	public static int percentage = 3;
}