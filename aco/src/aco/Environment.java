package aco;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Random;
import java.util.stream.Collectors;
import parser.lom.Graph;
import parser.lom.ScoreModule;

/**
 * Represents the solution space. Contains the vertex and edges of the graph to
 * be optimized and the pheromone let by the ants
 */
public class Environment {

	/**
	 * NxN matrix that contains the weight of the arcs
	 */
	private double[][] arcWeight;

	/**
	 * The data contained in each node of the graph
	 */
	private ArrayList<Double> nodeData;

	/**
	 * The topic of each node of the graph
	 */
	private ArrayList<String[]> nodeTopic;

	/**
	 * The matrix of NxN nearest neighbor for each vertex of the graph
	 */
	private int[][] NNList;

	/**
	 * The matrix of NxNN indicating the pheromone deposited by the ants
	 */
	private double[][] pheromone;

	/**
	 * Map of N entries indicating the pheromones deposited by the ants in each
	 * node
	 */
	private HashMap<Integer, Double> nodePheromone;

	/**
	 * Map of N entries containing information of each node
	 */
	private HashMap<Integer, String> nodeInfo;

	/**
	 * The matrix of NxN storing the value of the best edges calculated using
	 * the pheromone amount and the quality of the edges
	 */
	private double[][] choiceInfo;

	/**
	 * The ants agent
	 */
	private Ant[] ants;

	/**
	 * Environment requires a graph to solve
	 *
	 * @param arcWeight
	 * @param graph
	 */
	public Environment(Graph graph) {
		super();
		this.nodeData = graph.getNodeData();
		this.nodeInfo = graph.getNodeInfo();
		this.arcWeight = graph.getArcWeight();
		this.nodeTopic = graph.getNodeTopic();
	}

	/**
	 * Create a list with the NN nearest neighbors of each vertex and keep they
	 * in a separate structure of dimension N x NN where N = population size,
	 * and NN = nearest neighbors size
	 */
	void generateNearestNeighborList() {
		NNList = new int[getNodesSize()][getNodesSize()];
		/*
		 * For each node of the graph, sort the nearest neighbors by weight arc
		 * and cut the list by the size NN
		 */
		for (int i = 0; i < getNodesSize(); i++) {
			Integer[] nodeIndex = new Integer[getNodesSize()];
			Double[] nodeData = new Double[getNodesSize()];
			for (int j = 0; j < getNodesSize(); j++) {
				nodeIndex[j] = j;
				nodeData[j] = getCost(i, j);
			}
			/*
			 * The edge of the current vertex with himself is let as last option
			 * to be selected to nearest neighbors list
			 */
			nodeData[i] = Collections.min(Arrays.asList(nodeData));
			Arrays.sort(nodeIndex, new Comparator<Integer>() {
				public int compare(final Integer o1, final Integer o2) {
					return Double.compare(nodeData[o1], nodeData[o2]);
				}
			});
			for (int r = (getNodesSize() - 1); r >= 0; r--) {
				NNList[i][r] = nodeIndex[r];
			}
		}
	}

	/**
	 * Create a population of k ants to search solutions in the environment,
	 * where k is the number of ants
	 */
	void generateAntPopulation() {
		ants = new Ant[getAntPopSize()];
		for (int k = 0; k < getAntPopSize(); k++) {
			ants[k] = new Ant(getNodesSize(), this);
		}
	}

	/**
	 * Create pheromone and choice info structure: -> Pheromone is used to
	 * represent the quality of the edges used to build solutions. -> ChoiceInfo
	 * is calculated with the pheromone and the quality of routes, to be used by
	 * the ants as decision rule and index to speed up the algorithm
	 *
	 * To generate the environment the pheromone of each arc is initialized to a
	 * random value in the range (0, 1]
	 */
	void generateEnvironment() {
		pheromone = new double[getNodesSize()][getNodesSize()];
		choiceInfo = new double[getNodesSize()][getNodesSize()];
		nodePheromone = new HashMap<>();
		double initialTrail = 0.0;
		Random randomNo = new Random();
		for (int i = 0; i < getNodesSize(); i++) {
			for (int j = i; j < getNodesSize(); j++) {
				if (j == i) {
					pheromone[i][j] = 0.0;
					pheromone[j][i] = 0.0;
				} else {
					do {
						initialTrail = randomNo.nextDouble();
					} while (initialTrail <= 0.0);
					pheromone[i][j] = initialTrail;
					pheromone[j][i] = initialTrail;
				}
				choiceInfo[i][j] = 0.0;
				choiceInfo[j][i] = 0.0;
			}
			nodePheromone.put(i, 0.0);
		}
		calculateChoiceInformation();
	}

	/**
	 * Calculate the proportional probability of an ant at vertex i select a
	 * neighbor j based on the (i->j) edge cost (taken the inverse cost of the
	 * edge) and (i->j) edge pheromone amount. The parameters alpha and beta
	 * control the balance between heuristic and pheromone
	 */
	private void calculateChoiceInformation() {
		for (int i = 0; i < getNodesSize(); i++) {
			for (int j = 0; j < i; j++) {
				if (j == i) {
					choiceInfo[i][j] = 0.0;
				} else {
					choiceInfo[i][j] = Math.pow(pheromone[i][j], Parameters.alpha)
							* Math.pow(nodeData.get(j), Parameters.beta);
					choiceInfo[j][i] = Math.pow(pheromone[j][i], Parameters.alpha)
							* Math.pow(nodeData.get(i), Parameters.beta);
				}
			}
		}
	}

	/**
	 * Put each ant to construct a solution in the environment
	 */
	void constructSolutions() {
		/*
		 * At the first step reset all ants (clearVisited) and put each one in a
		 * random vertex of the graph
		 */
		int phase = 0;
		int aux = 0;
		Random randomNo = new Random();
		for (int k = 0; k < getAntPopSize(); k++) {
			ants[k].clearVisited();
			ants[k].startAtRandomPosition(phase);
		}
		/*
		 * Make all ants choose the next non visited vertex based in the
		 * pheromone trails and heuristic of the edge cost
		 */
		outerloop: for (int k = 0; k < getAntPopSize(); k++) {
			phase = 0;
			while (phase < getNodesSize() - 1) {
				aux = randomNo.nextInt(100);
				if (aux >= Parameters.percentage) {
					phase++;
					ants[k].goToNNListAsDecisionRule(phase);
				} else {
					ants[k].finishTour(phase);
					continue outerloop;
				}

			}
			ants[k].finishTour(phase);
		}
	}

	/**
	 * Update the pheromone taking into account the quality of the solutions
	 * build by the ants and the evaporation rate
	 */
	void updatePheromone() {
		evaporatePheromone();
		for (int k = 0; k < getAntPopSize(); k++) {
			depositPheromone(ants[k]);
		}
		calculateChoiceInformation();
	}

	/**
	 * Evaporate the amount of pheromone by a exponential factor (1 - rho) for
	 * all edges and nodes
	 */
	private void evaporatePheromone() {
		for (int i = 0; i < getNodesSize(); i++) {
			for (int j = i; j < getNodesSize(); j++) {
				pheromone[i][j] = (1 - Parameters.rho) * pheromone[i][j];
				pheromone[j][i] = pheromone[i][j];
			}
			double oldPheromone = nodePheromone.get(i);
			nodePheromone.put(i, ((1 - Parameters.rho) * oldPheromone));
		}
	}

	/**
	 * For the ant, deposit the amount of pheromone in all edges and nodes used
	 * in the ant where the amount of pheromone deposited is proportional to the
	 * solution quality
	 *
	 * @param ant
	 */
	private void depositPheromone(Ant ant) {
		int aux = ant.getRoutePhase(0);
		double oldPheromone = nodePheromone.get(aux);
		nodePheromone.put(aux, oldPheromone + ant.getTourCost());
		for (int i = 0; i < ant.getTourLength() - 1; i++) {
			int j = ant.getRoutePhase(i);
			int l = ant.getRoutePhase(i + 1);
			oldPheromone = nodePheromone.get(l);
			pheromone[j][l] = pheromone[j][l] + ant.getTourCost();
			pheromone[l][j] = pheromone[j][l];
			nodePheromone.put(l, oldPheromone + ant.getTourCost());
		}
	}

	/**
	 * After all the iterations get the best tour of all tours constructed by
	 * the ants, calculated by pheromone levels, show the values
	 *
	 * @param traces
	 * @param topic
	 */
	void calculateStatistics(int traces, String[] topic) {
		int counter = 0;
		Integer maxTrace = null;
		double maxValue = 0.0;
		final HashMap<Integer, Double> sortedMap = nodePheromone.entrySet().stream()
				.sorted((HashMap.Entry.<Integer, Double>comparingByValue().reversed())).collect(Collectors
						.toMap(HashMap.Entry::getKey, HashMap.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

		System.out.println("\nFINAL SOLUTION: ");
		for (HashMap.Entry<Integer, Double> pair : sortedMap.entrySet()) {
			if (counter >= traces) {
				break;
			} else {
				if (nodeData.get(pair.getKey()) > maxValue) {
					maxValue = nodeData.get(pair.getKey());
					maxTrace = pair.getKey();
				}
				counter++;
			}
			System.out.println("Game: " + pair.getKey() + " '" + nodeInfo.get(pair.getKey()) + "'"
					+ " Pheromone Level: " + pair.getValue());
		}
		counter = 0;
		String[] resource = nodeTopic.get(maxTrace);
		for (HashMap.Entry<Integer, Double> pair : sortedMap.entrySet()) {
			if (counter >= traces) {
				break;
			} else {
				counter++;
			}
			if (pair.getKey() == maxTrace) {
				continue;
			} else {
				resource[2] += " " + nodeTopic.get(pair.getKey())[2];
				resource[3] += "," + nodeTopic.get(pair.getKey())[3];
				resource[5] += "," + nodeTopic.get(pair.getKey())[5];
				if (resource[6] != null && nodeTopic.get(pair.getKey())[6] != null) {
					resource[6] = ScoreModule.averageString(nodeTopic.get(pair.getKey())[6], resource[6]);
				}
				if (resource[8] != null && nodeTopic.get(pair.getKey())[8] != null) {
					resource[8] = ScoreModule.sumString(nodeTopic.get(pair.getKey())[8], resource[8]);
				}
				resource[11] += "," + nodeTopic.get(pair.getKey())[11];
				resource[12] += "," + nodeTopic.get(pair.getKey())[12];
				resource[13] += "," + nodeTopic.get(pair.getKey())[13];
			}
		}
		// String stats = String.format("Min(%.1f) Max(%.1f) Mean(%.1f)\n", min,
		// max, (total / counter));
		if (traces > 1) {
			System.out.print("\nSIMILARITY RATING AFTER FUSING " + traces + " SUBTRACES: "
					+ ScoreModule.compare(topic, resource) + "\n");
		}
	}

	/**
	 * Return the number of nodes
	 *
	 * @return graphLength
	 */
	int getNodesSize() {
		return nodeData.size();
	}

	/**
	 * Return the distance between to vertices
	 *
	 * @param from
	 * @param to
	 * @return cost
	 */
	private double getCost(int from, int to) {
		return arcWeight[from][to];
	}

	/**
	 * Return the quality of a node in the graph
	 *
	 * @param index
	 * @return quality
	 */
	double getQuality(int index) {
		return nodeData.get(index);
	}

	/**
	 * Return the ant population size
	 *
	 * @return antPopSize
	 */
	private int getAntPopSize() {
		return Parameters.antPopSize;
	}

	/**
	 * Return the nearest neighbor of the index rank position
	 *
	 * @param from
	 * @param index
	 * @return targetVertex
	 */
	int getNNNode(int from, int index) {
		return this.NNList[from][index];
	}

	/**
	 * Return the heuristic-pheromone value of the edge
	 *
	 * @param from
	 * @param to
	 * @return costInfo
	 */
	double getCostInfo(int from, int to) {
		return choiceInfo[from][to];
	}
}