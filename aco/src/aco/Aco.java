package aco;

import parser.lom.LomParser;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import parser.lom.Graph;

/**
 * ACO - Ant Colony Optimization Meta-heuristic This algorithm represents the
 * implementation of ACO for Serious Emerging Games
 */
public class Aco {

	/**
	 * Start the ACO algorithm for the given files, topic and number of traces
	 * of the final solution
	 *
	 * @param filespath
	 * @param topic
	 * @param traces
	 */
	public void startAlgorithm(String filesPath, String[] topic, int traces) {
		Graph graph = LomParser.parseFiles(filesPath, topic);
		if (graph != null) {
			HashMap<Integer, String> auxMap = graph.getNodeInfo();
			ArrayList<Double> auxList = graph.getNodeData();
			ArrayList<String[]> auxTopic = graph.getNodeTopic();
			Double maxValue = Collections.max(auxList);
			if (maxValue >= Parameters.maxScore) {
				Integer maxId = auxList.indexOf(maxValue);
				System.out.println("The game: " + auxMap.get(maxId) + " with a score of: " + maxValue
						+ " meets the desired topic");
			} else {
				double[][] arcWeight = new double[auxList.size()][auxList.size()];
				for (int i = 0; i < auxList.size(); i++) {
					for (int j = 0; j < auxList.size(); j++) {
						if (i == j) {
							arcWeight[i][j] = 1.0;
						} else {
							arcWeight[i][j] = (1.0 - (Math.abs(auxList.get(i) - auxList.get(j))));
						}
					}
				}
				/* Startup part */
				Environment environment = new Environment(new Graph(auxMap, auxList, arcWeight, auxTopic));
				environment.generateNearestNeighborList();
				environment.generateAntPopulation();
				environment.generateEnvironment();

				/* Repeat the ants behavior by n times */
				int n = 0;
				while (n < Parameters.iterationsMax) {
					environment.constructSolutions();
					environment.updatePheromone();
					n++;
				}

				environment.calculateStatistics(traces, topic);
				}
				try {
					Thread.sleep(3000);
				} catch (Exception ex) {
				System.out.println("Finished");
			}

		} else {
			System.out.println("Finished");
		}
	}
}