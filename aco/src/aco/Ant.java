package aco;

/**
 * Represents the ant agent. It process their own state transition rules in the
 * environment
 */
public class Ant {

	/**
	 * Cost of the current path, it is the sum of all edges distances
	 */
	private double tourCost;

	/**
	 * Current tour, contains a sequence of edges that was traveled by the ant.
	 * Start at the first node and ends at the same node, ex: [A,B,C,D,E,A]
	 */
	private int[] tour;

	/**
	 * Current tour length
	 */
	private int tourLength;

	/**
	 * All nodes that were visited by the ant
	 */
	private boolean[] visited;

	/**
	 * Let the information about the edges, vertices and pheromones accessible
	 */
	private Environment environment;

	/**
	 * Create an adapted ant to the environment
	 *
	 * @param tourSize
	 * @param environment
	 */
	public Ant(int tourSize, Environment environment) {
		super();
		this.tour = new int[tourSize + 1];
		this.visited = new boolean[tourSize];
		this.environment = environment;
	}

	/**
	 * Reset the ant to be processed
	 */
	void clearVisited() {
		for (int i = 0; i < visited.length; i++) {
			visited[i] = false;
		}
	}

	/**
	 * Put the ant in a random vertex and mark it as visited
	 *
	 * @param phase
	 */
	void startAtRandomPosition(int phase) {
		tour[phase] = (int) (Math.random() * environment.getNodesSize());
		visited[tour[phase]] = true;
	}

	/**
	 * Sum all the quality values of the nodes visited by the ant in the tour
	 *
	 * @return tourQuality
	 */
	double computeTourQuality() {
		double tourQuality = 0.0;
		for (int i = 0; i < tourLength; i++) {
			tourQuality += environment.getQuality(tour[i]);
		}
		return tourQuality / tourLength;
	}

	/**
	 * Finish the ant tour through the graph, bind the last vertex with first
	 * vertex (last vertex n is equal to vertex 0) and calculate the route
	 * quality
	 * 
	 * @param phase
	 */
	void finishTour(int phase) {
		tour[phase + 1] = tour[0];
		tourLength = phase + 1;
		tourCost = computeTourQuality();
	}

	/**
	 * Calculate next neighbor tending probabilistically selecting the edge with
	 * more pheromone and smaller distance. First is tried to find a vector to
	 * move in the nearest neighbor list, but if all vectors were visited then
	 * is selected the best in all remaining neighbors
	 *
	 * @param phase
	 */
	void goToNNListAsDecisionRule(int phase) {
		/* Get the current city */
		int currentCity = this.tour[phase - 1];
		double sumProbabilities = 0.0;
		/*
		 * Vector of nearest neighbor probabilities proportional to the fitness
		 * of the edge
		 */
		double[] selectionProbabilities = new double[environment.getNodesSize() + 1];
		/*
		 * For each nearest neighbor vertex that was not visited yet add their
		 * fitness to the probability vector
		 */
		for (int j = 0; j < environment.getNodesSize(); j++) {
			if (visited[environment.getNNNode(currentCity, j)]) {
				selectionProbabilities[j] = 0.0;
			} else {
				selectionProbabilities[j] = environment.getCostInfo(currentCity, environment.getNNNode(currentCity, j));
				sumProbabilities += selectionProbabilities[j];
			}
		}
		/* Select the best neighbor */
		goToBestNeighbor(phase, sumProbabilities);
		return;
	}

	/**
	 * Select the best non visited neighbor of the current vertex (the best
	 * neighbor have the greater fitness calculated from pheromone and
	 * heuristic)
	 * 
	 * @param phase
	 * @param sumProbabilities
	 */
	private void goToBestNeighbor(int phase, double sumProbabilities) {
		int helpCity;
		int nextCity = environment.getNodesSize();
		/* Take the current city */
		int currentCity = this.tour[phase - 1];
		/* Start the best with a value that never will be achieved */
		double valueBest = -1.0;
		double help;
		/* Select the non visited neighbor with the maximum fitness */
		for (int i = 0; i < environment.getNodesSize(); i++) {
			helpCity = environment.getNNNode(currentCity, i);
			if (!this.visited[helpCity]) {
				help = (environment.getCostInfo(currentCity, helpCity)) / sumProbabilities;
				if (help > valueBest) {
					valueBest = help;
					nextCity = helpCity;
				}
			}
		}
		/* Move to the vertex */
		tour[phase] = nextCity;
		visited[this.tour[phase]] = true;
	}

	/**
	 * Return the cost of the current tour
	 *
	 * @return tourCost
	 */
	double getTourCost() {
		return tourCost;
	}

	/**
	 * Return the vertex for a specific phase of the travel
	 *
	 * @param phase
	 * @return vertex
	 */
	int getRoutePhase(int phase) {
		return tour[phase];
	}

	/**
	 * Return the current tour
	 * 
	 * @return tour
	 */
	int[] getTour() {
		return tour;
	}

	/**
	 * Return the current tour length
	 * 
	 * @return tourLength
	 */
	int getTourLength() {
		return tourLength;
	}
}